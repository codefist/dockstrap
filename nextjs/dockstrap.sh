#!/usr/bin/env bash

DOCKERFILE=$(cat <<HEREDOC
FROM alpine

RUN apk add --update yarn

RUN mkdir /app
WORKDIR /app
HEREDOC
)

DOCKER_COMPOSE=$(cat <<HEREDOC
version: '3'

services:
  app:
    build: .
    volumes:
      - .:/app
    ports:
      - 3000:3000
    command:
      yarn run dev
HEREDOC
)

SCRIPTS=$(cat <<HEREDOC
{\n  "scripts": {\n    "dev":  "next",\n    "build": "next build",\n    "start": "next start"\n  },
HEREDOC
)
# write the files
echo "$DOCKERFILE" > Dockerfile
echo "$DOCKER_COMPOSE" > docker-compose.yml

DC=docker-compose
$DC build --pull app
$DC run --rm app yarn init -y
$DC run --rm app yarn add react react-dom next

$DC run --rm app mkdir pages

# add the scripts section to package.json
$DC run --rm app sed -i "1s/./$SCRIPTS/" package.json

$DC up
