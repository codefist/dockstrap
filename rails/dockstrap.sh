#!/usr/bin/env bash

DATABASE_YML=$(cat <<HEREDOC
default: &default
  adapter: postgresql
  encoding: unicode
  host: postgres
  username: postgres
  password: postgres
  pool: 5
development:
  <<: *default
  database: app_development
test:
  <<: *default
  database: app_test
HEREDOC
)

DOCKERFILE=$(cat <<HEREDOC
FROM alpine
RUN apk add --update build-base \
 linux-headers \
 libxml2-dev \
 libxslt-dev \
 ruby-bundler \
 ruby-dev \
 ruby-rdoc \
 ruby-bigdecimal \
 postgresql-dev \
 tzdata \
 gnu-libiconv-dev \
 yarn

RUN mkdir /app
WORKDIR /app

RUN gem install rails -v '~> 6'

ENV BUNDLE_PATH /bundle
ENV BUNDLE_JOBS 2
ENV PATH $PATH:/bundle/bin

RUN bundle config --global path /bundle

# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]
HEREDOC
)

DOCKER_COMPOSE=$(cat <<HEREDOC
version: "3"

volumes:
  pg-data:
  bundle:

services:
  web:
    build: .
    command: bundle exec rails s -p 3000 -b '0.0.0.0'
    volumes:
      - .:/app
      - bundle:/bundle
    ports:
      - "3000:3000"
    depends_on:
      - postgres

  postgres:
    image: postgres
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: postgres
      PGDATA: /var/lib/postgresql/data
    volumes:
      - pg-data:/var/lib/postgresql/data
HEREDOC
)

# write the files
echo "$DOCKERFILE" > Dockerfile
echo "$DOCKER_COMPOSE" > docker-compose.yml

docker-compose build --pull web
docker-compose up -d --pull postgres
docker-compose run --rm web rails new . --force --git --database=postgresql
docker-compose run --rm web echo "$DATABASE_YML" > config/database.yml
docker-compose run --rm web bundle exec rails webpacker:install
docker-compose run --rm web bundle exec rails db:create
docker-compose up
